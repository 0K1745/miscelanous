import java.util.Arrays;
import java.util.stream.IntStream;

public class Sort {

    public static void main(String[] args) {

        int[] nums2 = {2, 3, 4, 5, 2};
        final var ints = new int[]{2, 3, 4, 5, 2};
        int[] nums1 = new int[nums2.length + ints.length];
        int index=0;
        for(int num : ints){
            nums1[index++]=num;
        }
        System.out.println("Sort:" + Arrays.toString(nums1) + Arrays.toString(nums2));
        mergeThenSort(nums1, ints.length, nums2);
        System.out.println("Sorted:" + Arrays.toString(nums1));
    }

    private static void mergeThenSort(int[] nums1, int m, int[] nums2){
        int i = 0;
        for (int num: nums2) {
            nums1[m+i++]=num;
        }
        Arrays.sort(nums1);
    }

    private static void quicksort(int[] nums1, int length, int[] nums2, int length1) {
        final var array = IntStream.concat(Arrays.stream(nums1).limit(length), Arrays.stream((nums2))).sorted().toArray();
        for (int i = 0; i < length+length1; i++) {
            nums1[i]=array[i];
        }
    }


}

public class Jump {


    public static void main(String[] args) {

        System.out.println(canJump(new int[]{2,5,1,1,1,2,5}));
        System.out.println(canJump(new int[]{0}));
        System.out.println(canJump(new int[]{2,5,0,0}));
        System.out.println(canJump(new int[]{3,2,1,0,4}));
    }

    public static boolean canJump(int[] nums) {
        int reachable = 0;
        for (int i = 0; i < nums.length; ++i) {
            if (i > reachable) return false;
            reachable = Math.max(reachable, i + nums[i]);
        }
        return true;
    }

    public static boolean canJumpMine(int[] nums) {
        for (int index = 0; index < nums.length - 1; ) {
            System.out.println(index);
            if (nums[index] == 0) {
                int zeroIndex = index;
                for (; index >= 0; index--) {
                    System.out.println(index);
                    if (index == 0) {
                        return false;
                    }
                    if (index + nums[index] > zeroIndex) {
                        break;
                    }
                }
            }
            index += nums[index];
        }
        return true;
    }

}

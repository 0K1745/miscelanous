import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Rotate {

    public static void main(String[] args) {
        final var nums = new int[]{1, 2};
        rotate(nums,2);
        System.out.println(Arrays.toString(nums));
        rotate(nums,3);
        System.out.println(Arrays.toString(nums));

    }

    public static void rotate(int[] nums, int k ){
        System.out.println("ROTATE: " + Arrays.toString(nums));
        if(nums.length <= 1){
            return;
        }
        int size = nums.length-1;
        int pivot = k>nums.length?(k%nums.length):k;
        System.out.println("pivot: " + pivot);
        reverse(nums, 0, size);
        reverse(nums, 0, pivot - 1 );
        reverse(nums, pivot, size );
    }

    public static void reverse(int[] nums, int start, int end){
        while (start<end){
            int shift=nums[start];
            nums[start]=nums[end];
            nums[end]=shift;
            start++;
            end--;
        }
    }
}

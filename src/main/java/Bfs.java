import java.util.ArrayDeque;
import java.util.Queue;

public class Bfs {
//    https://leetcode.com/problems/jump-game-ii/?envType=study-plan-v2&envId=top-interview-150
    public int minJumps(int[] nums) {
        int n = nums.length;
        if (n <= 1) {
            return 0;
        }

        Queue<Integer> queue = new ArrayDeque<>();
        queue.offer(0);

        int jumps = 0;
        boolean[] visited = new boolean[n];
        visited[0] = true;

        while (!queue.isEmpty()) {
            int size = queue.size();

            for (int i = 0; i < size; i++) {
                int currIndex = queue.poll();

                if (currIndex == n - 1) {
                    return jumps;
                }

                // Jump to indices with the same value
                for (int j = currIndex + 1; j < n; j++) {
                    if (nums[j] == nums[currIndex] && !visited[j]) {
                        queue.offer(j);
                        visited[j] = true;
                    }
                }

                // Explore forward jump
                int nextIndex = currIndex + nums[currIndex];
                if (nextIndex < n && !visited[nextIndex]) {
                    queue.offer(nextIndex);
                    visited[nextIndex] = true;
                }

                // Explore backward jump
                int prevIndex = currIndex - 1;
                if (prevIndex >= 0 && !visited[prevIndex]) {
                    queue.offer(prevIndex);
                    visited[prevIndex] = true;
                }
            }

            jumps++;
        }

        return -1; // In case there's no way to reach the destination
    }

    public static void main(String[] args) {
        Bfs bfs = new Bfs();
        int[] nums = {2, 3, 1, 1, 4};
        System.out.println(bfs.minJumps(nums));  // Output: 2
    }
}
